# RuboCop Training Agenda

## Prerequisites:

[Install GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/doc/index.md#install-and-configure-gdk)
Read [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/239356)

# Agenda

## What is RuboCop?

RuboCop is a Ruby code style checker (linter) and formatter based on the community-driven [Ruby Style Guide](https://rubystyle.guide/).

## Why would we care?

Apart from reporting problems in your code, RuboCop can also automatically fix some of the problems for you (which is the goal of the previously mentioned [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/239356))

## Problem

[`.rubocop_todo/**/*.yml`](https://gitlab.com/gitlab-org/gitlab/-/blob/mater/.rubocop_todo) contains auto-correctable cop offenses (`grep -hr auto-correct .rubocop_todo -A 1 | grep "^[A-Z]"`) which can be fixed automatically.

## Workflow

Resolve auto-correctable rules which are **enabled** by letting RuboCop auto-correct them automatically 🎉

1. Verify that the cop you are working on is already **enabled**. See also [#369268](https://gitlab.com/gitlab-org/gitlab/-/issues/369268 "RuboCop: Enable previously disabled cop")
	1. `cop` or 👮  is a script defining what is wrong, what is right, and how to correct it, it has a name like e.g. `Rails/WhereExists`
2. Pick a 👮 offense from [The List](https://gitlab.com/gitlab-org/gitlab/-/issues/239356#the-list)  e.g. `Rails/WhereExists`
3. Create a branch (e.g. `239356-fix-Rails/WhereExists)
    - Info: Using this issue ID (`239356`) in a branch name will add required labels and references to this issue in the new merge request.
4. Delete the rule (rules) from the corresponding `.rubocop_todo/*/*` YAML file (e.g. https://gitlab.com/gitlab-org/gitlab/-/blob/master/.rubocop_todo/rails/where_exists.yml)
5. Auto-correct RuboCop offenses via e.g. `bundle exec rubocop --auto-correct --only Layout/ClosingHeredocIndentation`
6. Check changed files and adjust if needed
7. Commit and create a merge request
    - Hint: You can use `Fix <Cop/Name> offenses` as git commit message    
8. Mention th merge request in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/239356)
9. 🎉

## Is it really that simple?

Yes, but...

##Where's the catch?

... you may end up with pipeline errors, failed tests etc. We will talk about some reasons for these failures, however regardless what caused the problem, you can still count on your reviewer or #mr-buddies to help you.
